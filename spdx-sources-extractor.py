# spdx-sources-extractor.py
# Copyright (C) 2021 J. Manrique López de la Fuente
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python
# coding: utf-8

"""spdx-sources-extractor.py: given a SPDX file in rdf format, it extracts
the download location for the packages listed
"""

__author__ = "J. Manrique López de la Fuente"
__copyright__ = "Copyright (C) 2021, J. Manrique López de la Fuente"
__license__ = "GPL"

__version__ = "0.0.1"
__maintainer__ = "J. Manrique López de la Fuente"

import rdflib
import csv

import argparse
import configparser

def parse_args(args):
    parser = argparse.ArgumentParser(description = 'Get a csv file with all the download locations for a given SPDX file')
    parser.add_argument('-s', '--spdx', dest = 'spdxFile', help = 'SPDX file location', required = True)
    parser.add_argument('-o', '--output', dest = 'outputFile', help = 'Output CSV file name')

    return parser.parse_args()

def download_locations(spdxFile):
    graph = rdflib.Graph()
    try:
        graph.parse(spdxFile)
    except Exception as e:
        print("Parsing error: {}".format(e))

    locations = []

    for sub, pred, obj in graph.triples((None, rdflib.term.URIRef("http://spdx.org/rdf/terms#downloadLocation"), None)):
        locations.append([obj])
    
    return locations

def main(args):
    args = parse_args(args)

    locations = download_locations(args.spdxFile)

    if args.outputFile:
        with open(args.outputFile, mode='w') as csv_file:
            locations_writer = csv.writer(csv_file)
            for location in locations:
                locations_writer.writerow(location)
    else:
        print(locations)

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])